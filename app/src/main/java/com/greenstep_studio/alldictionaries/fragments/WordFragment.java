package com.greenstep_studio.alldictionaries.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenstep_studio.alldictionaries.R;
import com.greenstep_studio.alldictionaries.activities.MainActivity;
import com.greenstep_studio.alldictionaries.models.Word;
import com.greenstep_studio.alldictionaries.utils.ZipUtils;

import java.io.IOException;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class WordFragment extends Fragment {
    private static WordFragment instance;

    private AppCompatTextView tvWord;

    public static WordFragment newInstance() {
        instance = new WordFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_word,
                container, false);
        tvWord = (AppCompatTextView) view.findViewById(R.id.tv_word);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Word word = (Word) this.getArguments().getSerializable("word");
        if(word == null) return;

        ((MainActivity) getActivity()).setToolbarTitle(word.name.substring(0, 1).toUpperCase() + word.name.substring(1).toLowerCase());
        try {
            tvWord.setText(ZipUtils.decompress(word.description));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
