package com.greenstep_studio.alldictionaries.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.greenstep_studio.alldictionaries.R;
import com.greenstep_studio.alldictionaries.activities.MainActivity;
import com.greenstep_studio.alldictionaries.adapters.WordsAdapter;
import com.greenstep_studio.alldictionaries.models.Word;

import java.util.ArrayList;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class WordsListFragment extends ListFragment implements AdapterView.OnItemClickListener {
    public WordsAdapter adapter;
    private ArrayList<Word> words;
    private static WordsListFragment instance;
    private String title  = "";


    public static WordsListFragment newInstance() {
        instance = new WordsListFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_dics_fragment,
                container, false);

        return view;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        title = "";
        try {
            title = bundle.getString("search_word");
            ((MainActivity) getActivity()).setToolbarTitle(title);
            words = (ArrayList<Word>) bundle.getSerializable("array_words");

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(words != null && !words.isEmpty()) {
            adapter = new WordsAdapter(getActivity(), words);
            setListAdapter(adapter);
        } else Toast.makeText(getActivity(), "oshibka", Toast.LENGTH_SHORT).show();
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
        final String tag = "WORD";
        FragmentManager fm = getActivity().getSupportFragmentManager();
        Fragment f = fm.findFragmentByTag(tag);
        if (f == null || !f.isVisible()) {
            Bundle bundle = new Bundle();
            Log.d(this.getClass().getCanonicalName(), String.valueOf(words.get(position).id));
            bundle.putSerializable("word", words.get(position));
            FragmentTransaction ft = fm.beginTransaction();
            //ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            WordFragment wordFragment = WordFragment.newInstance();
            wordFragment.setArguments(bundle);
            ft.replace(R.id.main_container, wordFragment, tag);
            ft.addToBackStack(tag);
            ft.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setToolbarTitle(title);
    }
}
