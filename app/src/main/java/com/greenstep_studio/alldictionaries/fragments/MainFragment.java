package com.greenstep_studio.alldictionaries.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.greenstep_studio.alldictionaries.R;
import com.greenstep_studio.alldictionaries.activities.MainActivity;
import com.greenstep_studio.alldictionaries.models.Word;

import java.util.Random;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class MainFragment extends Fragment {


    private Button btnDics;
    private Button btnRandom;
    private Button btnFav;
    private Button btnSearch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main,
                container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnDics = (Button) view.findViewById(R.id.btn_dash_dics);
        btnRandom = (Button) view.findViewById(R.id.btn_dash_random);
        btnFav = (Button) view.findViewById(R.id.btn_dash_fav);
        btnSearch = (Button) view.findViewById(R.id.btn_dash_search);
        initButtons();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setToolbarTitle(getString(R.string.app_name));
    }

    private void initButtons() {
        btnDics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*final String tag = "DICS";
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                //ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                ft.replace(R.id.main_container, new DicsListFragment(), tag);
                ft.addToBackStack(tag);
                ft.commit();*/
                Toast.makeText(getContext().getApplicationContext(),
                        "Ожидайте в следующей версии", Toast.LENGTH_SHORT).show();
            }
        });
        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int WORDS_COUNT = 227273;
                Random random = new Random();
                int randId = random.nextInt(WORDS_COUNT) + 1;
                Word word = ((MainActivity) getActivity()).databaseHandler.getWord(randId);
                final String tag = "WORD";
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment f = fm.findFragmentByTag(tag);
                if (word != null && (f == null || !f.isVisible())) {
                    Bundle bundle = new Bundle();
                    Log.d(this.getClass().getCanonicalName(), word.name);
                    bundle.putSerializable("word", word);
                    FragmentTransaction ft = fm.beginTransaction();
                    //ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    WordFragment wordFragment = WordFragment.newInstance();
                    wordFragment.setArguments(bundle);
                    ft.replace(R.id.main_container, wordFragment, tag);
                    ft.addToBackStack(tag);
                    ft.commit();
                }
            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).callOnClickSearch();
            }
        });
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext().getApplicationContext(),
                        "Ожидайте в следующей версии", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
