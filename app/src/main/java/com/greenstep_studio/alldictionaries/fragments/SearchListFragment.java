package com.greenstep_studio.alldictionaries.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.greenstep_studio.alldictionaries.R;
import com.greenstep_studio.alldictionaries.activities.MainActivity;
import com.greenstep_studio.alldictionaries.adapters.SearchAdapter;
import com.greenstep_studio.alldictionaries.utils.PreferencesUtils;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class SearchListFragment extends ListFragment implements AdapterView.OnItemClickListener {
    private SearchAdapter adapter;
    private MainActivity.Callback callback;
    private AppCompatButton btnClear;
    private static SearchListFragment instance;

    public static SearchListFragment newInstance() {
        instance = new SearchListFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_search_fragment, container, false);
        btnClear = (AppCompatButton) view.findViewById(R.id.btn_clear_history);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new SearchAdapter(getActivity(), PreferencesUtils.getAllSearches());
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesUtils.clearSearches();
                refreshItems();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        callback.onItemClicked((String) adapter.getItem(position));
    }

    public void refreshItems() {
        adapter.updateData();
    }

    public void setCallback(MainActivity.Callback callback) {
        this.callback = callback;
    }
}