package com.greenstep_studio.alldictionaries.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class PreferencesUtils {
    private static final String SHARED_PREFS_FILE = "com.greenstep_studio.alldictionaries";
    private static final String SHARED_PREFS_SEARCH_QUEUE = "com.greenstep_studio.alldictionaries.search_queue";
    private static SharedPreferences prefs;

    public static void init(Context context) {
        prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
    }

    public static void putNewSearch(String query) {
        LinkedList<String> search_queue = getListString(SHARED_PREFS_SEARCH_QUEUE);

        if(search_queue.contains(query)) search_queue.removeFirstOccurrence(query);
        search_queue.addFirst(query);
        while(search_queue.size() > 10) {
            search_queue.removeLast();
        }
        putListString(SHARED_PREFS_SEARCH_QUEUE, search_queue);
    }

    private static LinkedList<String> getListString(String key) {
        return new LinkedList<>(Arrays.asList(TextUtils.split(
                prefs.getString(key, ""), "‚_‚")));
    }

    private static void putListString(String key, Queue<String> stringList) {
        String[] myStringList = stringList
                .toArray(new String[stringList.size()]);
        prefs.edit()
                .putString(key, TextUtils.join("‚_‚", myStringList)).apply();
    }
    public static LinkedList<String> getAllSearches() {
        return getListString(SHARED_PREFS_SEARCH_QUEUE);
    }

    public static void clearSearches() {
        prefs.edit().remove(SHARED_PREFS_SEARCH_QUEUE).apply();
    }
}
