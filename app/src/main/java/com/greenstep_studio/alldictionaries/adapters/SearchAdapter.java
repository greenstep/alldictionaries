package com.greenstep_studio.alldictionaries.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.greenstep_studio.alldictionaries.utils.PreferencesUtils;

import java.util.LinkedList;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class SearchAdapter extends BaseAdapter {
    private LinkedList<String> mData;
    private Context mContext;

    public SearchAdapter(final Context context, final LinkedList<String> mData) {
        this.mData = mData;
        this.mContext = context;
    }

    public LinkedList<String> getData() {
        return mData;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = convertView;
        ViewHolder holder;
        if (convertView == null) {
            v = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) v.findViewById(android.R.id.text1);
            holder.name.setTextColor(Color.BLACK);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.name.setText(mData.get(position));

        return v;
    }

    public void updateData() {
        mData = PreferencesUtils.getAllSearches();
        notifyDataSetChanged();
    }

    private class ViewHolder {
        public TextView name;
    }
}