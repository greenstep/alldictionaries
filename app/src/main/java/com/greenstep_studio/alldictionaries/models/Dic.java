package com.greenstep_studio.alldictionaries.models;

import java.io.Serializable;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class Dic implements Serializable {
    public int id;
    public String name;
    public byte[] description;

    public Dic(int id) {
        this.id = id;
    }

    public Dic(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
