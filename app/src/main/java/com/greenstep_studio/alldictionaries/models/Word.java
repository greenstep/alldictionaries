package com.greenstep_studio.alldictionaries.models;

import java.io.Serializable;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class Word implements Serializable {
    public int id;
    public String name;
    public byte[] description;

    public Dic dic;

    public Word() {
    }

    public Word(int id, String name, byte[] description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Word(int id, Dic dic, String name, byte[] description) {
        this.id = id;
        this.dic = dic;
        this.name = name;
        this.description = description;
    }
}
