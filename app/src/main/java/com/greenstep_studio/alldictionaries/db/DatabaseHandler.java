package com.greenstep_studio.alldictionaries.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.greenstep_studio.alldictionaries.models.Dic;
import com.greenstep_studio.alldictionaries.models.Word;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.File;
import java.util.ArrayList;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class DatabaseHandler extends SQLiteAssetHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 8;

    // Database Name
    private static final String DATABASE_NAME = "db.sqlite";

    // table name
    private static final String TABLE_WORDS = "words";
    private static final String TABLE_DICS = "dics";

    // Words Table Columns names
    private static final String KEY_WORDS_ID = "id";
    private static final String KEY_WORDS_DIC_ID = "dic_id";
    private static final String KEY_WORDS_NAME = "name";
    private static final String KEY_WORDS_DESC = "description";

    // Dics Table Columns names
    private static final String KEY_DICS_ID = "id";
    private static final String KEY_DICS_NAME = "name";
    private static final String KEY_DICS_DESC = "description";

    private class LoadDatabaseTask extends AsyncTask<Context, Void, Void> {
        Context mContext;
        ProgressDialog mDialog;
        LoadDatabaseTask(Context context) {
            super();
            mContext = context;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new ProgressDialog(mContext);
            mDialog.setMessage("Загрузка базы данных...");
            mDialog.setCancelable(false);
            mDialog.show();
        }
        @Override
        protected Void doInBackground(Context... contexts) {
            // Copy database.
            getReadableDatabase();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mDialog.dismiss();
        }
    }

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();
        File database = context.getDatabasePath(DATABASE_NAME);
        if (!database.exists()) {
            new LoadDatabaseTask(context).execute(context, null, null);
        }
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    /*public int getWordsCount() {
        String selectQuery = "SELECT COUNT(id) FROM " + TABLE_WORDS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        int count = cursor.getInt(0);

        cursor.close();

        return count;
    }*/

    // Getting All Words
    /*public List<Word> getAllWords() {
        List<Word> wordList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT id, name FROM " + TABLE_WORDS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Word word = new Word();
                word.id = Integer.parseInt(cursor.getString(0));
                word.name = cursor.getString(1);
                //word.description = cursor.getBlob(2);
                // Adding word to list
                wordList.add(word);
            } while (cursor.moveToNext());
        }

        cursor.close();
        // return word list
        return wordList;
    }*/

    // Getting single word
    public Word getWord(int id) {
        Word word = new Word();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_WORDS, new String[]{KEY_WORDS_ID,
                        KEY_WORDS_NAME, KEY_WORDS_DESC}, KEY_WORDS_ID + "=" + id,
                null, null, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                word.id = cursor.getInt(0);
                Log.d(this.getClass().getCanonicalName(), String.valueOf(word.id));
                word.name = cursor.getString(1);
                word.description = cursor.getBlob(2);
            }
            cursor.close();
        } else word = null;

        return word;
    }

    public String getDic(int id) {
        String name = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_DICS, new String[]{
                        KEY_DICS_NAME}, KEY_DICS_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                name = cursor.getString(0);
            }
            cursor.close();
        }

        return name;
    }

    public ArrayList<Word> searchWord(String name) {
        ArrayList<Word> wordList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_WORDS, new String[]{KEY_WORDS_ID, KEY_WORDS_NAME, KEY_WORDS_DESC, KEY_WORDS_DIC_ID},
                KEY_WORDS_NAME + " MATCH ? ", new String[]{name + '*'}, null, null, null, null);
        // looping through all rows and adding to list
        if(cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Word word = new Word();
                    word.id = Integer.parseInt(cursor.getString(0));
                    word.name = cursor.getString(1);
                    word.description = cursor.getBlob(2);
                    int dic_id = cursor.getInt(3);
                    word.dic = new Dic(dic_id, getDic(dic_id));
                    // Adding word to list
                    wordList.add(word);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } else wordList = null;

        return wordList;
    }
}