package com.greenstep_studio.alldictionaries.activities;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.greenstep_studio.alldictionaries.R;

/**
 * Copyright (C) GreenStep Studio - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Egor Stepanov <eg.st.dev@greenstep-studio.com>, Февраль 2016.
 */
public class AboutActivity extends AppCompatActivity {
    TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        PackageInfo pInfo;
        String sVersion;
        tvVersion = (TextView)findViewById(R.id.tvVersion);
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            sVersion = pInfo.versionName;
            tvVersion.setText(getString(R.string.txt_version) + ": " + sVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
