package com.greenstep_studio.alldictionaries.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.androidsx.rateme.RateMeDialog;
import com.androidsx.rateme.RateMeDialogTimer;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.greenstep_studio.alldictionaries.R;
import com.greenstep_studio.alldictionaries.db.DatabaseHandler;
import com.greenstep_studio.alldictionaries.fragments.MainFragment;
import com.greenstep_studio.alldictionaries.fragments.SearchListFragment;
import com.greenstep_studio.alldictionaries.fragments.WordsListFragment;
import com.greenstep_studio.alldictionaries.models.Word;
import com.greenstep_studio.alldictionaries.utils.PreferencesUtils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {
    private SearchListFragment searchListFragment;

    private Menu menu;
    private SearchView searchView;
    private MenuItem searchViewItem;

    private InterstitialAd interstitialAd;

    public DatabaseHandler databaseHandler;

    public interface Callback {
        void onItemClicked(String name);
    }

    @Override
    protected void onStart() {
        super.onStart();
        final int launchTimes = 15;
        final int installDate = 15;
        RateMeDialogTimer.onStart(this);
        if (RateMeDialogTimer.shouldShowRateDialog(this, installDate, launchTimes)) {
            showCustomRateMeDialog();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHandler = new DatabaseHandler(this);
        handleIntent(getIntent());
        PreferencesUtils.init(getApplicationContext());
        initAdmob();

        //Listen for changes in the back stack
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        //Handle when activity is recreated like on orientation Change
        try {
            shouldDisplayHomeUp();
        } catch (NullPointerException e) {e.printStackTrace();}

        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_container, new MainFragment(), "MAIN")
                    .commit();
        }
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            final String tag = "WORDS";
            FragmentManager fm = getSupportFragmentManager();
            Fragment f = fm.findFragmentByTag(tag);

            if (f == null || !f.isVisible()) {
                String query = intent.getStringExtra(SearchManager.QUERY).toLowerCase();
                PreferencesUtils.putNewSearch(query);
                searchListFragment.refreshItems();
                ArrayList<Word> words =  databaseHandler.searchWord(query);
                if(words != null && !words.isEmpty()) {
                    // send words to wordsListFragment
                    menu.findItem(R.id.action_search).collapseActionView();
                    FragmentTransaction ft = fm.beginTransaction();
                    WordsListFragment wordsListFragment = WordsListFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("array_words", words);
                    bundle.putString("search_word", query);
                    wordsListFragment.setArguments(bundle);
                    //ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    ft.replace(R.id.main_container, wordsListFragment, tag);
                    ft.addToBackStack(tag);
                    ft.commit();
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu = m;
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.action_search), new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                getSupportFragmentManager().popBackStack();
                return true;
            }
        });

        // Associate searchable configuration with the SearchView
        final SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchViewItem = menu.findItem(R.id.action_search);
        searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String tag = "SEARCH";
                FragmentManager fm = getSupportFragmentManager();
                Fragment f = fm.findFragmentByTag(tag);

                if (f == null || !f.isVisible()) {
                    FragmentTransaction ft = fm.beginTransaction();
                    //ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    searchListFragment = SearchListFragment.newInstance();
                    searchListFragment.setCallback(new Callback() {
                        @Override
                        public void onItemClicked(String name) {
                            searchView.setQuery(name, true);
                        }
                    });
                    ft.replace(R.id.main_container, searchListFragment, tag);
                    ft.addToBackStack(tag);
                    ft.commit();
                }
            }
        });

        View searchPlate = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        if (searchPlate != null) {
            //searchPlate.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            EditText searchText = (EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            if (searchText != null) {
                //searchText.setTextColor(Color.WHITE);
                //searchText.setHintTextColor(Color.WHITE);
            }
        }

        return true;
    }

    public void callOnClickSearch() {
        //searchView.onActionViewExpanded();
        searchViewItem.expandActionView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id) {
            case R.id.action_about: {
                Intent aboutIntent = new Intent(this, AboutActivity.class);
                if (aboutIntent != null) {
                    startActivity(aboutIntent);

                    return true;
                }
                return false;
            }
            case R.id.action_share: {
                shareAction();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
        if(getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

    public void setToolbarTitle(String title) {
        if(getSupportActionBar() != null) getSupportActionBar().setTitle(title);
    }

    public boolean shareAction() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.txt_share));
        if (shareIntent != null) {
            Intent intent = Intent.createChooser(shareIntent, getString(R.string.txt_title_share));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            return true;
        }
        return false;
    }

    private void initAdmob() {
        interstitialAd = new InterstitialAd(getApplicationContext());
        interstitialAd.setAdUnitId("ca-app-pub-9219207349444904/3351842071");
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                //loadAd();
            }

            @Override
            public void onAdLoaded() {
                showInterstitial(0);
            }
        });
        loadAd();
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        interstitialAd.loadAd(adRequest);
    }

    public void showInterstitial(int delay) {
        new Handler(){}.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (interstitialAd.isLoaded())
                    interstitialAd.show();
            }
        }, delay);
    }

    private void showCustomRateMeDialog() {
        new RateMeDialog.Builder(getPackageName(), getString(R.string.app_name))
                .setHeaderBackgroundColor(ContextCompat.getColor(this, R.color.dialog_primary))
                .setBodyBackgroundColor(ContextCompat.getColor(this, R.color.dialog_primary_light))
                .setBodyTextColor(ContextCompat.getColor(this, R.color.dialog_primary))
                .enableFeedbackByEmail("contact@greenstep-studio.com")
                .showAppIcon(R.mipmap.ic_launcher)
                //.setShowShareButton(true)
                .setRateButtonBackgroundColor(ContextCompat.getColor(this, R.color.dialog_primary))
                .setRateButtonPressedBackgroundColor(ContextCompat.getColor(this, R.color.dialog_primary_dark))
                .setIconCloseColorFilter(ContextCompat.getColor(this, R.color.dialog_text_foreground))
                .setIconShareColorFilter(ContextCompat.getColor(this, R.color.dialog_text_foreground))
                .build()
                .show(getFragmentManager(), "custom-dialog");
    }
}
